package br.com.acesso.porta.repositories;
import br.com.acesso.porta.models.Porta;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public interface PortaRepository extends CrudRepository<Porta, Integer> {
    Optional<Porta> findById(int id);
}
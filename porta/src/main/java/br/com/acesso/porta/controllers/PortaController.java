package br.com.acesso.porta.controllers;

import br.com.acesso.porta.models.Porta;
import br.com.acesso.porta.services.PortaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController//transforma em classe controladora do spring
@RequestMapping("/porta")
public class PortaController {

    @Autowired
    private PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta cadastrarPorta(@RequestBody @Valid Porta porta){
        Porta objetoCliente = portaService.salvarPorta(porta);
        return objetoCliente;
    }

    @GetMapping("/{id}")
    public Porta pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Porta porta= portaService.buscarPortaPeloId(id);
            return porta;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
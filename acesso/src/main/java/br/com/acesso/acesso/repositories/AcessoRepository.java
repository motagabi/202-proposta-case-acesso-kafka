package br.com.acesso.acesso.repositories;

import br.com.acesso.acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Integer> {
    Optional<Acesso> findByClienteIdAndPortaId(long clienteId, long portaId);

    void deleteByClienteIdAndPortaId(long clienteId, long portaId);
}
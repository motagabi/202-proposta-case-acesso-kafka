package br.com.acesso.acesso.services;


import br.com.acesso.acesso.Clients.*;
import br.com.acesso.acesso.DTO.AcessoDTO;
import br.com.acesso.acesso.controllers.LogAcessoResponse;
import br.com.acesso.acesso.models.Acesso;
import br.com.acesso.acesso.repositories.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class AcessoService {
    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    @Autowired
    private AcessoProducer acessoProducer;

    public AcessoDTO salvarAcesso(AcessoDTO acessoDTO) {
        Cliente cliente = clienteClient.getById(acessoDTO.getClienteId());
        Porta porta = portaClient.getById(acessoDTO.getPortaId());

        if ((cliente != null) && (porta != null)) {
                Acesso acessoNovo = new Acesso();
                acessoNovo.setClienteId(acessoDTO.getClienteId());
                acessoNovo.setPortaId(acessoDTO.getPortaId());
                acessoRepository.save(acessoNovo);
                return this.transformarAcessoEmDTO(acessoNovo);
        } else {
            throw new RuntimeException("Dados inválidos - Cliente "+ cliente.getIdCliente() +" ou Porta "+ porta.getIdPorta() +" não encontrados para salvar o acesso!");
        }
    }

    private AcessoDTO transformarAcessoEmDTO(Acesso acesso) {
        AcessoDTO acessoDTO = new AcessoDTO();

        acessoDTO.setPortaId(acesso.getPortaId());
        acessoDTO.setClienteId(acesso.getClienteId());

        return acessoDTO;
    }
    public Acesso getByIdClienteAndByIdPorta(int clienteId , int portaId){

        //return acessoRepository.findByClienteIdAndPortaId(clienteId , portaId);
        try {
            Optional<Acesso> acesso = acessoRepository.findByClienteIdAndPortaId(clienteId,portaId);

            LogAcessoResponse logAcessoResponse = new LogAcessoResponse();
            logAcessoResponse.setClienteId(clienteId);
            logAcessoResponse.setPortaId(portaId);

            if (!acesso.isPresent()) {
                logAcessoResponse.setAcesso(false);
                acessoProducer.registrarLogKafka(logAcessoResponse);
                throw new ClienteNotFoundException();
            }
            logAcessoResponse.setAcesso(true);
            acessoProducer.registrarLogKafka(logAcessoResponse);
            return acesso.get();
        } catch (Exception e) {
            throw new RuntimeException("Erro ao consultar o acesso: " + e);
        }


    }
    /**
     * Excluir acesso com porta id e cliente id
     * @param portaId
     * @param clienteId
     */
    @Transactional
    public void excluirAcesso(long portaId, long clienteId) {
        try {
//            Acesso acesso = consultarAcesso(portaId, clienteId);
//            acessoRepository.deleteById(acesso.getId());
            acessoRepository.deleteByClienteIdAndPortaId(clienteId, portaId);
        } catch (Exception e) {
            throw new RuntimeException("Erro ao excluir o acesso: " + e);
        }
    }


}

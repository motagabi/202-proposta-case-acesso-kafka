package br.com.acesso.acesso.controllers;

import br.com.acesso.acesso.DTO.AcessoDTO;
import br.com.acesso.acesso.models.Acesso;
import br.com.acesso.acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController//transforma em classe controladora do spring
@RequestMapping("/acesso")
public class AcessoController {
    @Autowired
    AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoDTO cadastrarAcesso(@RequestBody @Valid AcessoDTO acessoDTO) {
        System.out.println("Cliente: " + acessoDTO.getClienteId());
        System.out.println("Cliente: " + acessoDTO.getPortaId());
        return acessoService.salvarAcesso(acessoDTO);
    }

    @GetMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.OK)
    public Acesso getByClienteIdPortaId(@PathVariable(name="cliente_id") int clienteId, @PathVariable(name="porta_id")  int portaId){
        return acessoService.getByIdClienteAndByIdPorta(clienteId, portaId);
    }
    @DeleteMapping("/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.OK)
    public void excluirAcesso(@PathVariable(name = "cliente_id") long clienteId,
                              @PathVariable(name = "porta_id") long portaId) {
        acessoService.excluirAcesso(portaId, clienteId);
    }
}
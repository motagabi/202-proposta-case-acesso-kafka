package br.com.acesso.acesso.controllers;

public class LogAcessoResponse {
    private int portaId;
    private int clienteId;
    private boolean acesso;

    public LogAcessoResponse() {
    }

    public int getPortaId() {
        return portaId;
    }

    public void setPortaId(int portaId) {
        this.portaId = portaId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public boolean isAcesso() {
        return acesso;
    }

    public void setAcesso(boolean acesso) {
        this.acesso = acesso;
    }
}

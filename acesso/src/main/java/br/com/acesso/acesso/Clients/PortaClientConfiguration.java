package br.com.acesso.acesso.Clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PortaClientConfiguration {
    @Bean
    public ErrorDecoder getPortaClientDecoder() {
        return new PortaClientDecoder();
    }
}

package br.com.acesso.cliente.exceptions;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteConfiguration {
    @Bean
    public ErrorDecoder getClienteDecoder(){
        return new ClienteDecoder();
    }

}

package br.com.acesso.cliente.service;

import br.com.acesso.cliente.exceptions.ClienteNotFoundException;
import br.com.acesso.cliente.models.Cliente;
import br.com.acesso.cliente.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;
    @Transactional
    public Cliente salvarCliente(Cliente cliente){
        Cliente objetoCliente = clienteRepository.save(cliente);
        return objetoCliente;
    }

    public Cliente buscarClientePeloId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return cliente;
        }else{
            throw new RuntimeException("O cliente "+ id +" não foi encontrado");
        }
    }
}
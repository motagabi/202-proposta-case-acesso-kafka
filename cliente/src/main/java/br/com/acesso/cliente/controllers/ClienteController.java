package br.com.acesso.cliente.controllers;

import br.com.acesso.cliente.models.Cliente;
import br.com.acesso.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController//transforma em classe controladora do spring
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente){
        Cliente objetoCliente = clienteService.salvarCliente(cliente);
        return objetoCliente;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Cliente cliente= clienteService.buscarClientePeloId(id);
            return cliente;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
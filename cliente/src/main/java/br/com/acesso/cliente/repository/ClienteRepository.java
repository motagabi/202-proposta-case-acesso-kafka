package br.com.acesso.cliente.repository;

import br.com.acesso.cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
    Optional<Cliente> findById(int id);
}